import requests
import os
print("Definiendo variables")


url='https://api.telegram.org/bot5494777783:'+os.environ["API_BOT_KEY"]+'/sendMessage'
url_deployed=''
send_gif=True
# Chats IDS :
# Test = "-442603074"
chat_id = "-442603074"
# Test 2 = "-643539051"
json_data={
    "chat_id":chat_id,
    "parse_mode": "HTML",
    "disable_web_page_preview": "true",
    "text": "<b>Pipeline completado 🎉🎉</b> \n\nÚltima versión desplegada: \n"+os.environ["CI_JOB_ID"]+" \n\nDesplegada en: \n <a href=\""+url_deployed+"\">"+url_deployed+"</a>\n\n(FrontEnd)\nCommit info:\n"+os.environ["CI_COMMIT_TITLE"]+"\n"+os.environ["CI_COMMIT_SHORT_SHA"]+""
}

r = requests.post(url,json_data)

if (send_gif):
  json_data_s={
      "chat_id":chat_id,
      "animation": "https://c.tenor.com/8ZDLU43omvcAAAAM/kid-thumbs-up.gif"
  }
  r = requests.post('https://api.telegram.org/bot5494777783:'+os.environ["API_BOT_KEY"]+'/sendAnimation',json_data_s)

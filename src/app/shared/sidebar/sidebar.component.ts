import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  urlSettings: string ="";
  urlWorkbook: string ="";

  constructor(private route: Router) {}


  items: MenuItem[] = [];

  activeItem: MenuItem = {};

  ngOnInit() {
    this.items = [];
    this.items = [
      {
        label: 'Configuración',
        icon: 'pi pi-cog',
        url: this.urlSettings,
      },
      {
        label: 'Libro de Campo',
        icon: 'pi pi-book',
        url: this.urlWorkbook,
      },
    ];
  }

  logout() {
    localStorage.removeItem('role');
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    this.route.navigate(['/login']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { rolesEnum } from 'src/app/core/models/users/rolesEnum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  urlAdmin: string = 'safecrops/user/';
  urlWorkbook: string = 'safecrops/workbook/';
  urlSettings: string = 'safecrops/settings';

  constructor(private route: Router) {}

  items: MenuItem[] = [];

  activeItem: MenuItem = {};

  ngOnInit() {
    this.items = [];
    this.items = [
      {
        label: 'Configuración',
        icon: 'pi pi-cog',
        url: this.urlSettings,
      },
      {
        label: 'Películas',
        icon: 'pi pi-book',
        url: this.urlWorkbook,
      },
    ];
  }

  logout() {
    localStorage.removeItem('role');
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    this.route.navigate(['/login']);
  }
}

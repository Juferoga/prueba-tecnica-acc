import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AccordionModule } from 'primeng/accordion';
import {FileUploadModule} from 'primeng/fileupload';

import { MenubarModule } from 'primeng/menubar';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {PasswordModule} from 'primeng/password';
import {InputNumberModule} from 'primeng/inputnumber';
import {KeyFilterModule} from 'primeng/keyfilter';
import {CalendarModule} from 'primeng/calendar';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {RadioButtonModule} from 'primeng/radiobutton';
import { CardModule} from 'primeng/card';

import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import {TooltipModule} from 'primeng/tooltip';
import {ToastModule} from 'primeng/toast';
import {SplitterModule} from 'primeng/splitter';
import { SidebarComponent } from './sidebar/sidebar.component';
import {MenuModule} from 'primeng/menu';


@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    AccordionModule,
    KeyFilterModule,
    MenubarModule,
    MenuModule,
    TieredMenuModule,
    FormsModule,
    ButtonModule,
    CalendarModule,
    DialogModule,
    DropdownModule,
    PasswordModule,
    RouterModule,
    InputNumberModule,
    InputTextareaModule,
    RadioButtonModule,
    FileUploadModule,
    CardModule,
    ToastModule,
    TooltipModule,
    SplitterModule
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    DialogModule,
    AccordionModule,
    ButtonModule,
    RadioButtonModule,
    ToastModule,
    TooltipModule,
    SplitterModule
  ],
})

export class SharedModule { }

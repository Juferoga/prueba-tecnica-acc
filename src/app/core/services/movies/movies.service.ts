import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie } from '../../models/movie/movies.model';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  headers = new HttpHeaders();

  constructor(private http:HttpClient) { }

  createMovie(movie: Movie):Observable<Movie>{

    var body = JSON.stringify(movie);

    return this.http.post<Movie>(
      environment.apiRoute + '/movies',
      body
    );
  }

  readMovie(key:string):Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/movies'
    );
  }
  readMovies():Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/movies'
    );
  }
  readMovieTitle(key:string):Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/search?query=' + key.toString()
    );
  }
  readMovieCountry(key:string):Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/search?query=' + key.toString()
    );
  }
  readMovieCalification(key:string):Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/search?query=' + key.toString()
    );
  }
  readMovieTop():Observable<Movie[]>{
    return this.http.get<Movie[]>(
      environment.apiRoute + '/top'
    );
  }

  updateMovie(movie: Movie):Observable<Movie>{
    var body = JSON.stringify(movie);
    return this.http.put<Movie>(
      environment.apiRoute + '/movies:'+movie.id,
      body
    )
  }
  deleteMovie(movie: Movie){
    return this.http.delete<Movie[]>(
      environment.apiRoute+'/movies/:'+movie.id,
    );
  }
}

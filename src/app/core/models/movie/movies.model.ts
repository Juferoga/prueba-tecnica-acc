export class Movie {
  id:number
  titulo:string
  calificacion:number
  pais:string

  constructor(){
    this.id = 0,
    this.titulo = '',
    this.calificacion = 0,
    this.pais = ''
  }
}

import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { Movie } from 'src/app/core/models/movie/movies.model';
//import { MoviesService } from 'src/app/core/services/movies/movies.service';
import MoviesJson from '../../../../../../assets/generated.json';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

    movieDialog: boolean = false;

    submitted: boolean = false;

    addDialog:boolean = false;

    movie1: Movie[]=[];

    movie2: Movie[]=[];

    statuses: SelectItem[]=[];

    clonedMovie: { [s: string]: Movie; } = {};

    constructor( private messageService: MessageService) { }

    ngOnInit() {
      this.movie1 = MoviesJson['data'];
      this.movie2 = MoviesJson['data'];

        this.statuses = [{label: 'Para niños', value: 10},{label: 'Mayores', value: 16},{label: 'Mayores de edad', value: 20}]
    }

    onRowEditInit(movie: Movie) {
        this.clonedMovie[movie.id] = {...movie};
    }

    onRowEditSave(movie: Movie) {
        if (movie.calificacion > 0) {
            delete this.clonedMovie[movie.id];
            this.messageService.add({severity:'success', summary: 'Éxito', detail:'El producto esta actualizado'});
        }
        else {
            this.messageService.add({severity:'error', summary: 'Error', detail:'Precio invalido'});
        }
    }

    onRowDelete(movie: Movie) {
      if (movie.calificacion > 0) {
          delete this.clonedMovie[movie.id];
          this.messageService.add({severity:'success', summary: 'Éxito', detail:'El producto esta actualizado'});
      }
      else {
          this.messageService.add({severity:'error', summary: 'Error', detail:'Precio invalido'});
      }
  }

    onRowEditCancel(movie: Movie, index: number) {
        this.movie2[index] = this.clonedMovie[movie.id];
        delete this.clonedMovie[movie.id];
    }

    display(){
      this.addDialog = true;
    }
}

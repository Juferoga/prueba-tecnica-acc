import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MoviesComponent } from './components/movies/movies.component';

import { MoviesRoutingModule } from './movie-routing.module';

import { ButtonModule } from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    MoviesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MoviesRoutingModule,
    ButtonModule,
    InputTextModule,
    ToastModule,
    TableModule,
    DropdownModule
  ]
})
export class MoviesModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from 'src/app/core/models/users/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  user: User = new User();

  constructor(private msgSvc: MessageService, private route: Router) {}
  ngOnInit(): void {}

  login() {
    if (this.user.username == '' && this.user.password == '') {
      this.msgSvc.add({
        key: 'general-toast',
        severity: 'error',
        summary: 'Ingreso Incorrecto',
        detail: 'Datos vacíos',
      });
    }
    if (
      this.user.username == 'Juferoga' &&
      this.user.password == 'Admin123..'
    ) {
      this.msgSvc.add({
        key: 'general-toast',
        severity: 'success',
        summary: 'Ingreso Correcto',
        detail: 'Bienvenido al sistema de películas',
      });
      this.route.navigate(['/acc/movies'])
    } else {
    }
    if (
      this.user.username == 'Juferoga' &&
      this.user.password == 'Usuario123..'
    ) {
      this.msgSvc.add({
        key: 'general-toast',
        severity: 'success',
        summary: 'Ingreso Correcto',
        detail: 'Bienvenido al sistema de películas',
      });
      this.route.navigate(['/acc/movies'])
    } else {
      this.msgSvc.add({
        key: 'general-toast',
        severity: 'error',
        summary: 'Ingreso Incorrecto',
        detail: 'Intente nuevamente',
      });
    }
  }
}
